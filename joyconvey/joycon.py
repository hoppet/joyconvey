#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

import hid
import itertools
from queue import Queue
from threading import Thread
from enum import Enum
from typing import List
from ctypes import c_uint8

from joyconvey.constants import JoyConPacket, NintendoHID, HIDEnumerateKeys

PACKET_LENGTH = 64


class JoyCon:
    class Chirality(Enum):
        UNKNOWN = 'Unknown'
        LEFT = "Left"
        RIGHT = "Right"

    def __init__(self, enumerated_info):
        """

        :param enumerated_info: A dictionary acquired from hidapi's enumerate_devices function
        """
        self.device_info = enumerated_info
        self._device = hid.device()
        self.global_packet_number: c_uint8 = c_uint8(0)
        self.input_queue = Queue()
        self._read_write_thread = self._DeviceReadWriteThread(self.input_queue, self._device)

    def get_chirality(self):
        """
        Get the chirality (left or right) of the Joy-Con.

        :return: A value of the Chirality enum representing whether this is a left or right Joy-Con
        """
        return {
            NintendoHID.JOYCON_LEFT_PRODUCT_ID: JoyCon.Chirality.LEFT,
            NintendoHID.JOYCON_RIGHT_PRODUCT_ID: JoyCon.Chirality.RIGHT
        }.get(self.device_info.get(HIDEnumerateKeys.PRODUCT_ID, ""), JoyCon.Chirality.UNKNOWN)

    def activate(self, playernum: int):
        """
        Opens a stream to the Joy-Con HID device, activates a player light, and sends a little rumble

        :param playernum: the player number light to activate
        """

        # TODO - error if unable to open device
        self._device.open(self.device_info[HIDEnumerateKeys.VENDOR_ID],
                          self.device_info[HIDEnumerateKeys.PRODUCT_ID])
        self._device.set_nonblocking(0)
        # self._do_handshake()
        self._set_standard_report_mode()
        self.set_player_light(1)
        self._read_write_thread.start()

    def set_player_light(self, playernum: int):
        """
        Set the light on the JoyCon for the given playernum

        :param playernum: the player number to set a light for (1, 2, 3, or 4)
        """

        # Get the byte value for the light setting. It is a bitmask where the where the first nibble is for flashing,
        # the second 4 bits are for solid lights. The highest order bit of each nibble is player 4, the lowest player 1
        # e.g: 0001 0010 is flashing light for player one, solid light for player two
        light_byte = {
            1: 0x01,
            2: 0x02,
            3: 0x04,
            4: 0x08
        }[playernum]

        # 0x30 is the subcommand byte, which is followed by the light byte
        subcommand = [JoyConPacket.SubCommands.SET_PLAYER_LIGHTS, light_byte]
        self._send_subcommand(subcommand)

    def _set_standard_report_mode(self):
        self._send_subcommand([JoyConPacket.SubCommands.SET_INPUT_REPORT_MODE, 0x30])

    def _do_handshake(self):
        """
        TODO: Yoinked from mfosse's joycon driver. Can't figure out where he got that info from. Probably unneeded
            actually
        """
        self._device.write([0x80, 0x02])
        self._device.write([0x80, 0x03])
        self._device.write([0x80, 0x02])
        self._device.write([0x80, 0x04])

    def _send_subcommand(self, subcommand: List[int], rumble_data: List[int] = None):
        """
        Constructs and sends a command to the HID device

        TODO - use this as a reference for rumble stuff
        https://github.com/dekuNukem/Nintendo_Switch_Reverse_Engineering/issues/11#issuecomment-360379786

        :param subcommand: the subcommand to send in the command
        :param rumble_data: a list of eight bytes representing rumble data. Described here under "rumble data":
            https://github.com/dekuNukem/Nintendo_Switch_Reverse_Engineering/blob/master/bluetooth_hid_notes.md
        """

        # command packet structure:
        # first byte: 0x01 is the byte to signal we're sending a subcommand with some optional rumble data
        # second byte: packet number is a number incremented with every packet sent
        # next 8 bytes: rumble data
        # subsequent bytes: the subcommand
        # packets seem to need to be 64 bytes long. The reverse engineering docs aren't too clear on this sadly
        command = [JoyConPacket.Commands.RUMBLE_AND_SUBCOMMAND, self.global_packet_number.value]
        self.global_packet_number.value += 1


        # add provided rumble data, or if none, 8 0x00 bytes for rumble data.
        if rumble_data:
            command += rumble_data
        else:
            for _ in itertools.repeat(None, 8):
                command.append(0x00)

        # add the subcommand
        command += subcommand

        # pad the rest of the list out to be 64 bytes
        for _ in itertools.repeat(None, PACKET_LENGTH - len(command)):
            command.append(0x00)

        # send the command to the Joy-Con
        self._read_write_thread.output_queue.put(command)

    class _DeviceReadWriteThread(Thread):
        """
        Inner class for reading input from the HID, which comes in at a rate of 60hz (assuming ideal connection) and
        blocks a thread if nothing is on the stream
        """
        def __init__(self, input_queue: Queue, device: hid.device):
            Thread.__init__(self)
            self.input_queue = input_queue
            self.output_queue: Queue = Queue()
            self._device = device

        def run(self):
            while True:
                if not self.output_queue.empty():
                    self._device.write(self.output_queue.get())

                packet = self._device.read(PACKET_LENGTH)

                # If the packet is an input report, slam it onto the input queue to be read by our owner
                if packet[0] == JoyConPacket.Responses.STANDARD_INPUT_REPORT:
                    self.input_queue.put(packet)

                # TODO - figure out how to require numpy but only for debug
                # if os.environ['JOYCONVEY_DEBUG']:
                #     np.set_printoptions(formatter={'int': hex})
                #     print("got input! " + np.array2string(np.trim_zeros(np.array(packet))))

