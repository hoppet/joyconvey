#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

import statistics
from collections import namedtuple

from math import sqrt

from joyconvey.calibration_data import CalibrationData
from joyconvey.input_translator import InputTranslator
from joyconvey.input import Input
import vigemclient
from vigemclient import XUSBButton

TRIGGER_MIN = 0
TRIGGER_MAX = 255
STICK_MIN = -32768
STICK_MAX = 32767
# TODO: explain how I got this radius, possibly including a photo of my notes in the repo
XBOX_STICK_CIRCLE_RADIUS = 37279
DEADZONE_MAX = XBOX_STICK_CIRCLE_RADIUS * 0.1

class InputTranslatorX360(InputTranslator):
    _shared_client = vigemclient.Client()

    def __init__(self):
        self.target = vigemclient.TargetX360()
        InputTranslatorX360._shared_client.add_target(self.target)

    def translate(self, joycon_input: Input, calibration_left: CalibrationData, calibration_right: CalibrationData):
        report = vigemclient.XUSBReport()

        # First translate the button presses. This is a bitmask on the xbox controller side
        report.buttons = translate_buttons(joycon_input)

        # Translate the shoulder buttons
        # TODO - unit test for this? Might not be worth bothering
        # TODO - Figure out a way to let the user do something in the trigger analog range, maybe sr/sr + stick
        #        direction? The particular scenario I feel this would help that comes to mind is duels in Red Dead
        #        Redemption 2
        report.left_trigger = TRIGGER_MAX if joycon_input.zl else TRIGGER_MIN
        report.right_trigger = TRIGGER_MAX if joycon_input.zr else TRIGGER_MIN

        # Translate the sticks
        stick_translations = translate_sticks(joycon_input, calibration_left, calibration_right)

        def in_deadzone(x, y) -> bool:
            vector_magnitude = sqrt(x ** 2 + y ** 2)
            return vector_magnitude < DEADZONE_MAX

        # Get whether we're in deadzone range on either stick
        # TODO: Probably better to do deadzone before translation to fix the stick going wild on startup
        left_in_deadzone = in_deadzone(stick_translations.x_left, stick_translations.y_left)
        right_in_deadzone = in_deadzone(stick_translations.x_right, stick_translations.y_right)

        report.thumb_left_x = int(stick_translations.x_left) if not left_in_deadzone else 0
        report.thumb_left_y = int(stick_translations.y_left) if not left_in_deadzone else 0
        report.thumb_right_x = int(stick_translations.x_right) if not right_in_deadzone else 0
        report.thumb_right_y = int(stick_translations.y_right) if not right_in_deadzone else 0

        # Send the report!
        self.target.update(report)


def translate_buttons(joycon_input: Input) -> int:
    # A mapping of each joycon button press to the xbox button it should translate to
    button_mapping = [
        (joycon_input.b, XUSBButton.a),
        (joycon_input.a, XUSBButton.b),
        (joycon_input.y, XUSBButton.x),
        (joycon_input.x, XUSBButton.y),
        (joycon_input.minus, XUSBButton.back),
        (joycon_input.plus, XUSBButton.start),
        (joycon_input.left, XUSBButton.left),
        (joycon_input.right, XUSBButton.right),
        (joycon_input.up, XUSBButton.up),
        (joycon_input.down, XUSBButton.down),
        (joycon_input.l, XUSBButton.left_shoulder),
        (joycon_input.r, XUSBButton.right_shoulder),
        (joycon_input.home, XUSBButton.guide),
        (joycon_input.stick_button_left, XUSBButton.left_thumb),
        (joycon_input.stick_button_right, XUSBButton.right_thumb)
    ]

    # For each of our joycon buttons, if it was pressed, add the corresponding xbox button to the buttons bitmask
    buttons = 0
    for joycon_button, xbox_button in button_mapping:
        if joycon_button:
            buttons |= xbox_button

    return buttons


StickTranslations = namedtuple("StickTranslations", "x_left, y_left, x_right, y_right")


def translate_sticks(joycon_input: Input,
                     calibration_left: CalibrationData,
                     calibration_right: CalibrationData) -> StickTranslations:
    left_scale_factor = get_scale_factor(calibration_left)
    right_scale_factor = get_scale_factor(calibration_right)

    x_left = (joycon_input.stick_left_x - calibration_left.center.x) * left_scale_factor
    y_left = (joycon_input.stick_left_y - calibration_left.center.y) * left_scale_factor
    x_right = (joycon_input.stick_right_x - calibration_right.center.x) * right_scale_factor
    y_right = (joycon_input.stick_right_y - calibration_right.center.y) * right_scale_factor

    def clamp(value):
        return min(value, STICK_MAX) if value >= 0 else max(value, STICK_MIN)

    return StickTranslations(
        x_left=clamp(x_left),
        y_left=clamp(y_left),
        x_right=clamp(x_right),
        y_right=clamp(y_right)
    )


def get_scale_factor(calibration: CalibrationData) -> float:
    xmax_magnitude = calibration.x_max - calibration.center.x
    factor_xmax = XBOX_STICK_CIRCLE_RADIUS / xmax_magnitude if xmax_magnitude else 0

    xmin_magnitude = calibration.center.x - calibration.x_min
    factor_xmin = XBOX_STICK_CIRCLE_RADIUS / xmin_magnitude if xmin_magnitude else 0

    ymax_magnitude = calibration.y_max - calibration.center.y
    factor_ymax = XBOX_STICK_CIRCLE_RADIUS / ymax_magnitude if ymax_magnitude else 0

    ymin_magnitude = calibration.center.y - calibration.y_min
    factor_ymin = XBOX_STICK_CIRCLE_RADIUS / ymin_magnitude if ymin_magnitude else 0

    return statistics.mean([factor_xmax, factor_xmin, factor_ymax, factor_ymin])
