#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

class NintendoHID:
    VENDOR_ID = 1406
    JOYCON_LEFT_PRODUCT_ID = 8198
    JOYCON_RIGHT_PRODUCT_ID = 8199


class HIDEnumerateKeys:
    PRODUCT_ID = "product_id"
    VENDOR_ID = "vendor_id"


class JoyConPacket:
    class Commands:
        RUMBLE_AND_SUBCOMMAND = 0x01
        RUMBLE_ONLY = 0x10


    class SubCommands:
        SET_INPUT_REPORT_MODE = 0x03
        SET_PLAYER_LIGHTS = 0x30


    class Responses:
        STANDARD_INPUT_REPORT = 0x30


# Masks for each bit in a byte
Bits = [0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80]
