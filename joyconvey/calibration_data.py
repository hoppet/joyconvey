#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

import attr
from collections import namedtuple

Point = namedtuple("Point", "x y")

"""
An object representing calibration data for Joy-Cons. All default values for fields are rough averages gotten from
observed Joy-Con input data.
"""
@attr.s(frozen=True)
class CalibrationData:
    # The center coordinates of the stick. The defaults represent average values from observed data.
    center: Point = attr.ib(Point(2000, 2000))

    # The maximum values each axis. This represents
    x_max: int = attr.ib(3000)
    y_max: int = attr.ib(3000)
    x_min: int = attr.ib(300)
    y_min: int = attr.ib(300)

    def get_average_radius(self) -> int:
        x_max_magnitude = self.x_max - self.center.x
        y_max_magnitude = self.y_max - self.center.y
        x_min_magnitude = self.center.x - self.x_min
        y_min_magnitude = self.center.y - self.y_min

        return (x_max_magnitude + y_max_magnitude + x_min_magnitude + y_min_magnitude) / 4
