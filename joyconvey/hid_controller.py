#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

import hid
from joyconvey.constants import NintendoHID, HIDEnumerateKeys
from joyconvey.joycon import JoyCon


def get_joycons() -> list:
    """
    Get all joycons connected to our machine.

    :return: An Iterator containing JoyCon objects, each representing one Joy-Con
    """

    joycon_infos = []

    # Grab all connected Nintendo devices
    for dev in hid.enumerate(NintendoHID.VENDOR_ID):
        for key in sorted(dev.keys()):
            print("%s : %s" % (key, dev[key]))
        print()

        # If it's a JoyCon we know about, create a JoyCon object for it
        product_id = dev[HIDEnumerateKeys.PRODUCT_ID]
        if(product_id == NintendoHID.JOYCON_RIGHT_PRODUCT_ID
                or product_id == NintendoHID.JOYCON_LEFT_PRODUCT_ID):
            joycon_infos.append(dev)

    return list(map(JoyCon, joycon_infos))
