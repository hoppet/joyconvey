#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

from joyconvey.hid_controller import get_joycons
from joyconvey.joycongroup import group_joycons
from joyconvey.joycongroup import JoyConGroup
from joyconvey.input_translator_x360 import InputTranslatorX360

ERROR_COULD_NOT_COMBINE = "Could not find two Joy-Cons to combine. Please ensure you have a left and right Joy-Con " \
                          + "paired and are using the Windows Bluetooth Stack"


def on_jc_group(joycon_group: JoyConGroup):
    joycon_group.input_translator = InputTranslatorX360()
    joycon_group.activate()


def main():
    joycons = get_joycons()
    group_joycons(joycons).map_or_else(
        on_jc_group,
        lambda: print(ERROR_COULD_NOT_COMBINE))
