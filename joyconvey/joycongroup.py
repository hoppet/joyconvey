#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

from typing import List
from option import Option
from joyconvey.joycon import JoyCon
from threading import Thread
from joyconvey.input import Input
from joyconvey.input_translator import InputTranslator
from joyconvey import calibration_data
from joyconvey.calibration_data import CalibrationData
from collections import namedtuple


class JoyConGroup:
    def __init__(self, left: JoyCon, right: JoyCon):
        self.left_joycon: JoyCon = left
        self.right_joycon: JoyCon = right
        self._input_handler = self._InputHandler(self.left_joycon, self.right_joycon)

    def activate(self):
        self.left_joycon.activate(1)
        self.right_joycon.activate(1)
        self._input_handler.start()

    @property
    def input_translator(self) -> Option[InputTranslator]:
        # Todo: do this but thread safe
        return self._input_handler.input_translator

    @input_translator.setter
    def input_translator(self, input_translator: InputTranslator):
        self._input_handler.input_translator = Option.Some(input_translator)

    class _InputHandler(Thread):
        # How many iterations of input handling we should flush the queues
        FLUSH_TIME = 120

        CalibrationTuple = namedtuple('CalibrationTuple', 'left right')

        def __init__(self, left: JoyCon, right: JoyCon):
            Thread.__init__(self)
            self.left_joycon = left
            self.right_joycon = right
            self.input_translator: Option[InputTranslator] = Option.NONE()
            self.joycon_calibration: Option[self.CalibrationTuple] = Option.NONE()

            # How many times we've called _get_input. Used to keep track when to flush the input queues
            self._read_iterations = 0

        def run(self):
            while True:
                left_input, right_input = self._get_input()
                combined_input = Input.from_joycon_data(left_input, right_input)

                # TODO: _calibrate is potentially inefficient. Profile to determine if this method is too expensive to
                #       call every input iteration. For now let's not pre-optimize if it isn't necessary.
                self._calibrate(combined_input)
                self._translate(combined_input)

        # TODO: Figure out how to nicely refactor this out of this already kinda big subclass. Also there's a lot of
        #       repeated code for left and right here but I haven't figured out how to consolidate it without making
        #       an unreadable mess of passing function pointers around
        def _calibrate(self, joycon_input: Input):
            if not self.joycon_calibration.is_some:
                # If this is our first input, use it as our center position, and use the center position as our min and
                # max values as well
                center_left = calibration_data.Point(joycon_input.stick_left_x, joycon_input.stick_left_y)
                center_right = calibration_data.Point(joycon_input.stick_right_x, joycon_input.stick_right_y)
                calibration_left = CalibrationData(center=center_left,
                                                   x_min=center_left.x, x_max=center_left.x,
                                                   y_min=center_left.y, y_max=center_left.y)
                calibration_right = CalibrationData(center=center_right,
                                                    x_min=center_right.x, x_max=center_right.x,
                                                    y_min=center_right.y, y_max=center_right.y)
                self.joycon_calibration = Option.Some(self.CalibrationTuple(calibration_left, calibration_right))
                return

            # Get the center point of our sticks (it's common practice to assume the first joysctick position you
            # got is your "neutral" position. Then get the new maxes and mins for each axis' direction
            calibrations = self.joycon_calibration.unwrap()
            center_left = calibrations.left.center
            center_right = calibrations.right.center
            left_x_min = min(calibrations.left.x_min, joycon_input.stick_left_x)
            left_x_max = max(calibrations.left.x_max, joycon_input.stick_left_x)
            left_y_min = min(calibrations.left.y_min, joycon_input.stick_left_y)
            left_y_max = max(calibrations.left.y_max, joycon_input.stick_left_y)
            right_x_min = min(calibrations.right.x_min, joycon_input.stick_right_x)
            right_x_max = max(calibrations.right.x_max, joycon_input.stick_right_x)
            right_y_min = min(calibrations.right.y_min, joycon_input.stick_right_y)
            right_y_max = max(calibrations.right.y_max, joycon_input.stick_right_y)

            calibration_left = CalibrationData(center=center_left,
                                               x_min=left_x_min, x_max=left_x_max,
                                               y_min=left_y_min, y_max=left_y_max)
            calibration_right = CalibrationData(center=center_right,
                                                x_min=right_x_min, x_max=right_x_max,
                                                y_min=right_y_min, y_max=right_y_max)
            self.joycon_calibration = Option.Some(self.CalibrationTuple(calibration_left, calibration_right))

        def _translate(self, joycon_input: Input):
            if self.input_translator.is_some and self.joycon_calibration.is_some:
                input_translator = self.input_translator.unwrap()
                calibration = self.joycon_calibration.unwrap()
                input_translator.translate(joycon_input, calibration.left, calibration.right)

        def _get_input(self) -> (List[int], List[int]):
            """
            Gets the next input from the left and right Joy-Con, occasionally flushing the input queues to make sure
            we've got the latest input
            :return: A tuple containing the input of the left Joy-Con at index 0, and the right at index 1
            """
            # Grab the next input for the left and right joycons
            if self._read_iterations < self.FLUSH_TIME:
                left_input = self.left_joycon.input_queue.get()
                right_input = self.right_joycon.input_queue.get()
                self._read_iterations += 1
            else:
                # Flush the input queues every now and then to ensure we're working with the most up-to-date data.
                # because Bluetooth is kind of unreliable this will help prevent inputs from bunching up and causing
                # button press delay for the player
                while not self.left_joycon.input_queue.empty():
                    self.left_joycon.input_queue.get(block=False)
                while not self.right_joycon.input_queue.empty():
                    self.right_joycon.input_queue.get(block=False)
                self._read_iterations = 0
                return self._get_input()
            return left_input, right_input


def group_joycons(joycons: List[JoyCon]) -> Option[JoyConGroup]:
    """
    Combines the joycons into a JoyConSet object to be treated as one controller.

    TODO - Currently it finds the first of a left and right joycon and combines them. In the future we want to support
           multiple sets of joycons

    :param joycons: a list of JoyCon objects
    :return: An Option containing the JoyConCombo
    """

    left_joycon = next(filter(lambda jc: jc.get_chirality() == JoyCon.Chirality.LEFT, joycons), None)
    right_joycon = next(filter(lambda jc: jc.get_chirality() == JoyCon.Chirality.RIGHT, joycons), None)

    if right_joycon is not None and left_joycon is not None:
        return Option.Some(JoyConGroup(left_joycon, right_joycon))
    else:
        return Option.NONE()

