#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

from typing import List
import attr
from joyconvey.constants import Bits


"""
An object representing a combined joycon input set for a given moment.
"""
@attr.s(frozen=True)
class Input:
    stick_left_x: int = attr.ib(default=0)
    stick_left_y: int = attr.ib(default=0)
    stick_right_x: int = attr.ib(default=0)
    stick_right_y: int = attr.ib(default=0)

    stick_button_left: bool = attr.ib(default=False)
    stick_button_right: bool = attr.ib(default=False)
    up: bool = attr.ib(default=False)
    down: bool = attr.ib(default=False)
    left: bool = attr.ib(default=False)
    right: bool = attr.ib(default=False)
    a: bool = attr.ib(default=False)
    b: bool = attr.ib(default=False)
    x: bool = attr.ib(default=False)
    y: bool = attr.ib(default=False)
    plus: bool = attr.ib(default=False)
    minus: bool = attr.ib(default=False)
    r: bool = attr.ib(default=False)
    zr: bool = attr.ib(default=False)
    l: bool = attr.ib(default=False)
    zl: bool = attr.ib(default=False)
    sl_left: bool = attr.ib(default=False)
    sr_left: bool = attr.ib(default=False)
    sl_right: bool = attr.ib(default=False)
    sr_right: bool = attr.ib(default=False)
    home: bool = attr.ib(default=False)
    capture: bool = attr.ib(default=False)

    @classmethod
    def from_joycon_data(cls, left_data: List[int], right_data: List[int]) -> 'Input':
        """
        Parse the input packets of two JoyCons into our combined input data

        Description of the packet contents can be found here under "Standard input report format:"
        https://github.com/dekuNukem/Nintendo_Switch_Reverse_Engineering/blob/master/bluetooth_hid_notes.md

        :param left_data: A standard input packet received from a left JoyCon's input queue
        :param right_data: A standard input packet received from a right JoyCon's input queue
        """

        """
        Get the bytes representing button input. The left and right Joy-Cons both send packets that contain fields for
        both Joy-Cons, sending 0 for buttons that the device doesn't have. Byte 3 is the right Joy-Con buttons, 4 is a
        set of bits shared between the two, and 5 is info for buttons on the left Joy-Con.
        """
        right_buttons = right_data[3]
        shared_buttons = left_data[4] | right_data[4]
        left_buttons = left_data[5]

        y = bool(right_buttons & Bits[0])
        x = bool(right_buttons & Bits[1])
        b = bool(right_buttons & Bits[2])
        a = bool(right_buttons & Bits[3])
        sr_right = bool(right_buttons & Bits[4])
        sl_right = bool(right_buttons & Bits[5])
        r = bool(right_buttons & Bits[6])
        zr = bool(right_buttons & Bits[7])

        # Only six bits for shared. Well bit 8 is "Charging Grip" but not important for our purposes
        minus = bool(shared_buttons & Bits[0])
        plus = bool(shared_buttons & Bits[1])
        r_stick_press = bool(shared_buttons & Bits[2])
        l_stick_press = bool(shared_buttons & Bits[3])
        home = bool(shared_buttons & Bits[4])
        capture = bool(shared_buttons & Bits[5])

        down = bool(left_buttons & Bits[0])
        up = bool(left_buttons & Bits[1])
        right = bool(left_buttons & Bits[2])
        left = bool(left_buttons & Bits[3])
        sr_left = bool(left_buttons & Bits[4])
        sl_left = bool(left_buttons & Bits[5])
        l = bool(left_buttons & Bits[6])
        zl = bool(left_buttons & Bits[7])

        # Left stick data come from bytes 6, 7, and 8
        left_stick = cls.get_stick_data(left_data[6:9])
        # Right stick data comes from bytes 9, 10, and 11
        right_stick = cls.get_stick_data(right_data[9:12])

        # TODO: Better logging than commented out print statements :P
        # print("lx: {} | ly: {} | rx: {} | ry: {}".format(left_stick[0], left_stick[1], right_stick[0], right_stick[1]))

        return cls(left_stick[0], left_stick[1], right_stick[0], right_stick[1], l_stick_press, r_stick_press, up, down,
                   left, right, a, b, x, y, plus, minus, r, zr, l, zl, sl_left, sr_left, sl_right, sr_right, home,
                   capture)

    @staticmethod
    def get_stick_data(stick_bytes: List[int]) -> (int, int):
        """
        Gets the stick x and y values from a set of three bytes holding the stick data. This algorithm is nabbed from
        https://github.com/dekuNukem/Nintendo_Switch_Reverse_Engineering/blob/master/bluetooth_hid_notes.md (search for
        "Standard input report - Stick data") and I am eternally grateful for whoever figured this out.

        :param stick_bytes: A set of three bytes representing Joy-Con stick data. On a standard Joy-Con input report,
            these are bytes 6, 7, 8 for left, and 9, 10, 11 for right.
        :return: A tuple with item 0 representing the x position, and 1 representing y
        """
        stick_x = stick_bytes[0] | ((stick_bytes[1] & 0xF) << 8)
        stick_y = (stick_bytes[1] >> 4) | (stick_bytes[2] << 4)
        return stick_x, stick_y
