#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

import pytest

from joyconvey.joycon import JoyCon
from unittest.mock import Mock, MagicMock, call


def test_gets_chirality_left(left_joycon):
    assert left_joycon.get_chirality() == JoyCon.Chirality.LEFT


def test_gets_chirality_right(right_joycon):
    assert right_joycon.get_chirality() == JoyCon.Chirality.RIGHT


def test_gets_chirality_empty(blank_joycon):
    assert blank_joycon.get_chirality() == JoyCon.Chirality.UNKNOWN


def test_gets_chirality_junk(junk_data_joycon):
    assert junk_data_joycon.get_chirality() == JoyCon.Chirality.UNKNOWN


def mock_device_write(joycon: JoyCon) -> Mock:
    mock_write = Mock()
    joycon._read_write_thread.output_queue.put = mock_write
    return mock_write


@pytest.mark.parametrize("player_num, light_byte", [(1, 0x01), (2, 0x02), (3, 0x04), (4, 0x08)])
def test_sets_player_light(left_joycon, player_num, light_byte):
    mock_write = mock_device_write(left_joycon)
    left_joycon.set_player_light(player_num)

    write_bytes = mock_write.call_args[0][0]
    assert write_bytes[0] == 0x01
    assert write_bytes[10] == 0x30
    assert write_bytes[11] == light_byte


def test_activates(left_joycon):
    mock_write = mock_device_write(left_joycon)
    left_joycon._device = Mock()
    mock_open = left_joycon._device.open = Mock()
    player_light_mock = left_joycon.set_player_light = Mock()

    left_joycon.activate(1)

    # Device should have been opened
    assert mock_open.called

    # Player light 1 should be activated
    assert player_light_mock.call_args[0][0] == 1

    # Hid should be sent a packet to change input report mode to full
    # TODO: This assert is a bit brittle because it assumes packet number, but the alternative is a lot of for loops.
    #   Maybe I can revisit this with a better algorithm later
    expected_input_report_packet = [0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x30]
    expected_input_report_packet.extend([0x00] * 52)
    assert call(expected_input_report_packet) in mock_write.call_args_list
