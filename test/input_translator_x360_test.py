#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

import pytest
from conftest import sample_inputs
from joyconvey.input_translator_x360 import translate_buttons

# Reference to button bitmasks: https://docs.microsoft.com/en-us/windows/desktop/api/XInput/ns-xinput-_xinput_gamepad
@pytest.mark.parametrize("joycon_input, expected_button_mask", [
    (sample_inputs[0], 0b0000000000010110),
    (sample_inputs[1], 0b0000001100001001),
    (sample_inputs[2], 0b0000000100100000),
    (sample_inputs[3], 0b0000000000000000),
    (sample_inputs[4], 0b0000010000000000),
    (sample_inputs[5], 0b0000000001000000),
    (sample_inputs[6], 0b0001001010000000),
    (sample_inputs[7], 0b1110000000000000)
])
def test_translates_buttons(joycon_input, expected_button_mask):
    buttons = translate_buttons(joycon_input)
    assert buttons == expected_button_mask
