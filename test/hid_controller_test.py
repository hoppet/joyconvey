from joyconvey import hid_controller
from joyconvey.constants import HIDEnumerateKeys
import pytest
import hid
from unittest.mock import Mock


@pytest.fixture
def nintendo_devices():
    device_dicts = [
        {HIDEnumerateKeys.PRODUCT_ID: 8198},
        {HIDEnumerateKeys.PRODUCT_ID: 8199},
        {HIDEnumerateKeys.PRODUCT_ID: 1337},
    ]
    for d in device_dicts:
        d[HIDEnumerateKeys.VENDOR_ID] = 1406

    return device_dicts


def test_get_joycons_only_gets_joycons(nintendo_devices):
    hid.enumerate = Mock(return_value=nintendo_devices)
    joycons = hid_controller.get_joycons()
    assert len(joycons) == 2