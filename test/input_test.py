#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

import json
import pytest
from conftest import sample_inputs
from joyconvey.input import Input


INPUT_SAMPLE_DIR = "test/input_samples/"


'''
The various samples taken for this test tried to cover a every button being pressed in at least one sample. When my
were capable of it joysticks were held in arbitrary directions too.
'''
@pytest.mark.parametrize("sample_l_file, sample_r_file, expected_input", [
    ("input_sample_0_l.json", "input_sample_0_r.json", sample_inputs[0]),
    ("input_sample_1_l.json", "input_sample_1_r.json", sample_inputs[1]),
    ("input_sample_2_l.json", "input_sample_2_r.json", sample_inputs[2]),
    ("input_sample_3_l.json", "input_sample_3_r.json", sample_inputs[3]),
    ("input_sample_4_l.json", "input_sample_4_r.json", sample_inputs[4]),
    ("input_sample_5_l.json", "input_sample_5_r.json", sample_inputs[5]),
    ("input_sample_6_l.json", "input_sample_6_r.json", sample_inputs[6])
])
def test_maps_input(sample_l_file: str, sample_r_file: str, expected_input: Input):
    with open(INPUT_SAMPLE_DIR + sample_l_file, 'r') as lf:
        sample_l = json.load(lf)
    with open(INPUT_SAMPLE_DIR + sample_r_file, 'r') as rf:
        sample_r = json.load(rf)

    parsed_input = Input.from_joycon_data(sample_l, sample_r)
    assert parsed_input == expected_input
