#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

from typing import List

import pytest

from joyconvey.constants import HIDEnumerateKeys
from joyconvey.joycon import JoyCon
from joyconvey.input import Input

'''
Each of the samples here up to 6 (counting from 0) mirror a corresponding pair of input files in the input_samples
directory.
'''
sample_inputs: List[Input] = [
    # Sample 0 - Down, Left, and Plus are pressed
    Input(stick_left_x=1971, stick_left_y=2097, stick_right_x=2112, stick_right_y=1849, down=True, left=True,
          plus=True),
    # Sample 1 - Up, Right, R, ZR, L, and ZL are pressed
    Input(stick_left_x=1989, stick_left_y=2118, stick_right_x=2264, stick_right_y=1778, up=True, right=True, r=True,
          zr=True, l=True, zl=True),
    # Sample 2 - L, SR (left), SR (left), and Minus are pressed
    Input(stick_left_x=1969, stick_left_y=2124, stick_right_x=2266, stick_right_y=1779, sr_left=True, sl_left=True,
          minus=True, l=True),
    # Sample 3 - SL (right) and Capture are pressed
    Input(stick_left_x=1989, stick_left_y=2134, stick_right_x=2245, stick_right_y=1860, sl_right=True, capture=True),
    # Sample 4 - SR (right) and Home pressed
    Input(stick_left_x=2009, stick_left_y=2144, stick_right_x=2229, stick_right_y=1805, sr_right=True, home=True),
    # Sample 5 - Stick left, ZL, SR (left) pressed
    Input(stick_left_x=915, stick_left_y=1888, stick_right_x=2165, stick_right_y=1790, stick_button_left=True, zl=True,
          sr_left=True),
    # Sample 6 - Stick right, B, R pressed
    Input(stick_left_x=1953, stick_left_y=2136, stick_right_x=1721, stick_right_y=2958, stick_button_right=True, b=True,
          r=True),
    # NOTE: This input is not represented by an input_sample file pair. I needed something with A, X, and Y pressed some
    #       time well after taking the samples
    Input(stick_left_x=1953, stick_left_y=2136, stick_right_x=1721, stick_right_y=2958, a=True, x=True, y=True)
]

@pytest.fixture
def left_joycon():
    return JoyCon({
        HIDEnumerateKeys.VENDOR_ID: 1406,
        HIDEnumerateKeys.PRODUCT_ID: 8198
    })


@pytest.fixture
def right_joycon():
    return JoyCon({
        HIDEnumerateKeys.VENDOR_ID: 1406,
        HIDEnumerateKeys.PRODUCT_ID: 8199
    })


@pytest.fixture
def blank_joycon():
    return JoyCon({})


@pytest.fixture
def junk_data_joycon():
    return JoyCon({
        HIDEnumerateKeys.VENDOR_ID: 1406,
        HIDEnumerateKeys.PRODUCT_ID: 133742069
    })
