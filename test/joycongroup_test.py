#  Copyright 2019 Holly Hoppet
#
#  This file is part of JoyConvey
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

from joyconvey.joycon import JoyCon
from joyconvey.joycongroup import group_joycons, JoyConGroup


def has_proper_chirality(group: JoyConGroup) -> bool:
    left_chirality_correct = group.left_joycon.get_chirality() == JoyCon.Chirality.LEFT
    right_chirality_correct = group.right_joycon.get_chirality() == JoyCon.Chirality.RIGHT
    return left_chirality_correct and right_chirality_correct


def test_group(left_joycon, right_joycon):
    assert group_joycons([left_joycon, right_joycon]).is_some
    assert group_joycons([left_joycon, right_joycon, left_joycon]).is_some


def test_chirality(left_joycon, right_joycon):
    assert group_joycons([left_joycon, right_joycon]).filter(has_proper_chirality).is_some


def test_doesnt_group_junk(left_joycon, right_joycon, junk_data_joycon):
    assert group_joycons([left_joycon, junk_data_joycon]).is_none
    assert group_joycons([junk_data_joycon, junk_data_joycon]).is_none
    assert group_joycons([junk_data_joycon, left_joycon, right_joycon]).filter(has_proper_chirality).is_some

