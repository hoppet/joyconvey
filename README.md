# JoyConvey

JoyConvey lets you use a pair of Nintendo Joy-Cons as a single controller on your Windows PC with full analogue support!
The project is fairly rudimentary right now but I'm happy to finally release it!

### Requirements

JoyConvey currently only supports the Windows native bluetooth stack. I might look into expanding this eventually
but it might be an ordeal as bluetooth is a bit of a pain.

This software requires ViGEmBus to be installed. See more info in "How to use"

### Where to get it

Swing on over to the [releases](https://gitlab.com/hoppet/joyconvey/-/releases) page to download. It's just a single exe file at least for now.

### How to use


1. Download and install ViGEmBus https://github.com/ViGEm/ViGEmBus/releases (scroll down to the "Assets" section). This
is the driver that lets us make a fake xbox controller.

2. Pair a single left and right Joy-Con to your Windows machine via bluetooth (the Windows bluetooth menu can be found
easily by searching the start menu for "bluetooth")

3. Run JoyConvey.exe and cross your fingers.

4. If your Joy-Cons' lights have stopped flashing and been set to player 1, then you did it!

5. Spin the sticks around a little to calibrate the program. For now, they'll act kind of wild before you do :P.

6. You can further verify by checking Windows' game controller control panel. An XBox 360 controller should show up there.


### Troubleshooting

Joy-Cons are notorious for having pretty crummy antennas, so you need a good bluetooth setup to compensate. If you
experience any input lag or disconnections try to make sure you have a direct line of sight from your Joy-Cons to your
bluetooth adapter. Before I got a "heavy duty" bluetooth dongle i would have to basically have my bluetooth dongle like
right next to my controllers on a USB extension cable sometimes. An internal bluetooth adapter may give you unsatisfactory
results, while one with a good antenna (ie sticking out of the thing) may help.

As mentioned before, only the Windows bluetooth stack is currently supported. If, in order to pair a bluetooth device,
you have to use something other than the native Windows bluetooth control panel, chances are you're not using the Windows
stack. Sometimes uninstalling the 3rd party software lets you use the controller as-is, but not in all cases.

### Contributions

Though this software is released under the GPL, I wish to be the sole copyright holder of the work in this repository for the time being as this is a portfolio piece for me, and I haven't settled on the full direction of the project or its licensing situation going forward. Eventually I'll be willing to accept contributions given the signage of a copyright transfer agreement, but for now I kinda want to keep the work to myself.

### Disclaimer

Nintendo's Joy-Cons do not have the best antennas in them. If you are experiencing disconnection issues or input lag--though it can help--I
cannot garuntee that purchasing a new bluetooth receiver of any sort will improve your results. I am not responsible for any expenses made
on hardware you may make related to this project.

Additionally, this software is provided "as-is." Though I will try when I can, I have no responsibility to provide maintenance, support,
updates, enhancements, or modifications. In no event shall I be liable for any damages, including lost profits, arising out of the use
of this software.
